# Read data from log-file
import matplotlib.pyplot as plt

import numpy as np
from matplotlib.animation import FuncAnimation


plt.style.use(['science','notebook', 'grid']) 

def report_iterator():
    '''Iterator filter function
    Opens text file with temperature data
    and ignore all lines until actual data
    then returns string content of each lines as an iterator
    '''
    with open("/home/jakob/Arduino/k-couple/ktemp.log", "r") as myfile:
        for line in myfile:
            if "Internal" in line:
                break
        for line in myfile:
            yield line

def calibrate(data, rawlow=1.0, rawhigh=101.25):
    '''
    Use previous 2-point calibration measurement
    to provide internal calibration of data. Update rawlow
    and rawhigh if you make new calibration.
    '''
    data = np.array(data)
    refLow = 0.01
    refHigh = 100
    refRange = refHigh - refLow
    rawRange = rawhigh - rawlow
    
    calibrated_data = np.around(((data - rawlow)*refRange)/rawRange + refLow, 1)
    
    return calibrated_data


def read_data():
    '''Convert strings from lines into lists of
    floats
    
    return time, k-couple temp, cold junction temp
    '''
    tInt = []
    temp = []
    for line in report_iterator():
        Temp = float(line.split(sep=",")[1])
        itemp = float(line.split(sep=",")[0])
        temp.append(Temp)
        tInt.append(itemp)
        
    t = np.linspace(0, 0.5*(len(temp)-1), len(temp))
    return t, temp, tInt


def animate(i):
    '''
    Standard animator function needed by
    FuncAnimation
    
    Plots temperature
    '''
    # read data from file
    t, temp, itemp = read_data()
    
    # comment out if you want uncalibrated data
    temp = calibrate(data=temp)
    
    # print values of current temp as label 
    s1 = "Temp: "+str(temp[-1])+r'$\degree$ C'
    s2 = "Cold Junction: "+str(round(itemp[-1], 1))+r'$\degree$ C'
    
    # clear current axes in every loop
    plt.cla()
    
    # create plot
    plt.plot(t, temp, label=s1)
    plt.plot(t, itemp, label=s2)
    plt.legend(loc="upper left", 
               facecolor = "white",
               fontsize=16)
    plt.ylim(min(min(itemp),min(temp)) - 1, max(max(itemp),max(temp)) + 3) 
    plt.ylabel(r'Temp / $\degree$ C')
    plt.xlabel('Tid / s')
    plt.tight_layout()
    
# interval is in miliseconds. Arduino is set to read and
# send data (approx) once every 500 ms at the moment. All that data is saved
# by Putty. It's often fine updating plot 1 time / second. 
# All the data will still show up from the log-file.
ani = FuncAnimation(plt.gcf(), animate, interval=1000)

plt.tight_layout()
plt.show()