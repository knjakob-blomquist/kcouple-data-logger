# k-type Thermocouple data logger and plotter using Arduino, Max31855 and Python

Temperature logger using k-type thermocouple connected to MAX31855 amplifier ver. 2 connected to arduino via serial port. 

# Project description
This is a plotter for k-type thermocouple data loggers. 
The thermocouple is connected to an arduino uno via an amplifier chip: MAX31855 ver. 2. The chip is provided with a cold-junction die which acts as a reference for the chip when it calculates volts -> temperature. 

The temperature is calculated via software in the arduinon (can use hardware conversion as well) and sent from the arduino via serial port to the laptop.

As soon as any software listens to the serial port, the program on the arduinon first starts a new test and then it sends data points (cold-junction temperature and thermocouple temperature respectively) 2 times/s, i.e. the data points are sent at 0.5 s intervals.

Putty reads the serial port and logs the data in a text file, ktemp.log.

The Python code/jupyter notebook reads the file and ignores all data linked to test and previous, and sends temperature data from elements and from cold-junction to a figure.


# Calibration 
I suggest a 2-point calibration with ice-water and boiling water.

## To perform a two point calibration:
Take two measurements with your sensor:  One near the low end of the measurement range and one near the high end of the measurement range.  Record these readings as "RawLow" and "RawHigh"
Repeat these measurements with your reference instrument.  Record these readings as "ReferenceLow" and "ReferenceHigh"
Calculate "RawRange" as RawHigh – RawLow.
Calculate "ReferenceRange" as ReferenceHigh – ReferenceLow
In your code, calculate the "CorrectedValue" using the formula below:

CorrectedValue = (((RawValue – RawLow) * ReferenceRange) / RawRange) + ReferenceLow

A common example of a two-point calibration is to calibrate a temperature sensor using an ice-water bath and boiling water for the two references.   Thermocouples and other temperature sensors are quite linear within this temperature range, so two point calibration should produce good resuts.

Since these are physical standards, we know that at normal sea-level atmospheric pressure, water boils at 100°C and the "triple point" is 0.01°C.  We can use these known values as our reference values:

ReferenceLow = 0.01°C

ReferenceHigh = 100°C

ReferenceRange = 99.99


So, if we get a raw reading of 37°C with this thermometer, we can plug the numbers into the equation to get the corrected reading:

(((37 + 0.5) * 99.99) / 96.5) + 0.01 = 38.9°C 

## License
MIT License

Copyright (c) 2022 Jakob Blomquist

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


